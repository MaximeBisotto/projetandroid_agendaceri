package fr.android.agendaceri.projetandroid_agendaceri;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Date;
import java.util.List;

import fr.android.agendaceri.projetandroid_agendaceri.iCal.DBEvenement;
import fr.android.agendaceri.projetandroid_agendaceri.iCal.Evenement;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DBEvenement dbEvenement = null;
        try {
            dbEvenement = new DBEvenement(this, getBaseContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Button buttonpreference = (Button) findViewById(R.id.buttonPref);
        buttonpreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, preference.class);
                startActivity(intent);
            }
        });
        Button buttonEDT = (Button) findViewById(R.id.buttonMesCours);
        buttonEDT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Edt.class);
                intent.putExtra("DATE", new Date());
                startActivity(intent);
            }
        });

        NotificationManager mNotification = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.this, 0, notificationIntent, 0);

        List<Evenement> tabEvent = dbEvenement.alerteEvaluation(new Date());
        for (Evenement event : tabEvent) {
            Notification.Builder builder = new Notification.Builder(getApplicationContext())
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle("alerte evaluation")
                    .setContentText("Matière : " + event.getMatiere())
                    .setContentIntent(contentIntent);

            mNotification.notify(100, builder.build());
        }
//        Log.d("DEBUG", dbEvenement.prochainCours().toString());
    }
}
