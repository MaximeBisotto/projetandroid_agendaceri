package fr.android.agendaceri.projetandroid_agendaceri.iCal;

import android.content.Context;
import android.text.Layout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Evenement {
    private String matiere;
    private String enseignant;
    private String lieu;
    private List<Integer> promotion;
    private String type;
    private String memo;
    private Date debut;
    private Date fin;

    public Evenement(String matiere, String enseignant, String lieu, List<Integer> promotion, String type, String memo, Date debut, Date fin) {
        this.matiere = matiere;
        this.enseignant = enseignant;
        this.lieu = lieu;
        this.promotion = promotion;
        this.type = type;
        this.memo = memo;
        this.debut = debut;
        this.fin = fin;
    }

    public String promoToString() {
        String chaine = "";
        for (int groupe : this.promotion) {
            chaine += groupe + ",";
        }
        return chaine.substring(0, chaine.length() - 2);
    }

    public static List<Integer> toPromotion(String string) {
        List<Integer> tabPromo = new ArrayList<>();
        String[] promo = string.split(",");
        for (String groupe : promo) {
            tabPromo.add(Integer.parseInt(groupe));
        }
        return tabPromo;
    }

    public String getMatiere() {
        return matiere;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public String getLieu() {
        return lieu;
    }

    public List<Integer> getPromotion() {
        return promotion;
    }

    public String getType() {
        return type;
    }

    public String getMemo() {
        return memo;
    }

    public Date getDebut() {
        return debut;
    }

    public Date getFin() {
        return fin;
    }

    @Override
    public String toString() {
        return "Evenement{" +
                "matiere='" + matiere + '\'' +
                ", enseignant='" + enseignant + '\'' +
                ", lieu='" + lieu + '\'' +
                ", promotion=" + promotion +
                ", type='" + type + '\'' +
                ", memo='" + memo + '\'' +
                ", debut=" + debut +
                ", fin=" + fin +
                '}';
    }

    public View toView(Context context) {
        TableLayout tableLayout = new TableLayout(context);
        tableLayout.setOrientation(LinearLayout.VERTICAL);

        TextView matiere = new TextView(context);
        matiere.setText(this.matiere);
        tableLayout.addView(matiere);

        TextView enseignant = new TextView(context);
        matiere.setText(this.enseignant);
        tableLayout.addView(enseignant);

        TextView debut = new TextView(context);
        matiere.setText(this.debut.toString());
        tableLayout.addView(debut);

        TextView fin = new TextView(context);
        matiere.setText(this.fin.toString());
        tableLayout.addView(fin);

        TextView lieu = new TextView(context);
        matiere.setText(this.lieu);
        tableLayout.addView(lieu);

        TextView type = new TextView(context);
        matiere.setText(this.type);
        tableLayout.addView(type);

        return tableLayout;
    }
}
