package fr.android.agendaceri.projetandroid_agendaceri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Date;

public class Edt extends AppCompatActivity {

    private Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edt);
        this.date = (Date) getIntent().getParcelableExtra("DATE");
        Button buttonPrecedant = (Button) findViewById(R.id.buttonPrec);
        buttonPrecedant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Edt.this, Edt.class);
                Date today = new Date();
                intent.putExtra("DATE", new Date(date.getTime() + (1000 * 60 * 60 * 24)));
                startActivity(intent);
            }
        });
        Button buttonSuivant = (Button) findViewById(R.id.buttonSuiv);
        buttonSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Edt.this, Edt.class);
                Date today = new Date();
                intent.putExtra("DATE", new Date(date.getTime() - (1000 * 60 * 60 * 24)));
                startActivity(intent);
            }
        });
    }
}
