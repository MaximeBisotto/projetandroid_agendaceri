package fr.android.agendaceri.projetandroid_agendaceri.iCal;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class RecupICal extends AsyncTask {

    private String urlString;
    private DBEvenement dbEvenement;
    private InputStream input;

    public RecupICal(String url, DBEvenement dbEvenement) {
        this.urlString = url;
        this.dbEvenement = dbEvenement;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            URL url = new URL(this.urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("GET");
            Log.d("DEBUG",urlString);
            urlConnection.connect();
            Log.d("DEBUG", "est connecte");
            this.input = new BufferedInputStream(urlConnection.getInputStream());
            Log.d("DEBUG", "a obtenu l'inputStream");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        doInBackground(null);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        try {
            String ligne = "";
            boolean verifieValidite = false, valide = false;
            Scanner sc = new Scanner(this.input);
            while (!verifieValidite) {
                ligne = sc.nextLine();
                if (ligne.split(":")[0].equals("X-CALEND")) {
                    if (DBEvenement.stringToDate(ligne.split(":")[1]).before(new Date(System.currentTimeMillis())))  {
                        Log.e("ERROR","le calendrier est fini, la date de fin est dans le passé");
                    }
                }
            }
            if (valide) {
                while (sc.hasNextLine()) {
                    ligne = sc.nextLine();
                    if (ligne.equals("BEGIN:VEVENT")) {
                        BuilderEvenement buildEvent = new BuilderEvenement();
                        ligne = sc.nextLine();
                        boolean annuler = false;
                        while (!ligne.equals("END:VEVENT")) {
                            annuler = false;
                            if (ligne.split(":")[0].contains("DESCRIPTION")) {
                                ligne = ligne.substring(13);
                                if (ligne.substring(0, 10).contains("ANNULATION")) {
                                    annuler = true;
                                } else {
                                    String[] tabCategorie = ligne.split("\n");
                                    for (String categorieString : tabCategorie) {
                                        String[] categorie = categorieString.split(":");
                                        if (categorie[0].contains("Matière")) {
                                            if (categorie.length == 3) {
                                                categorie[1] += categorie[2];
                                            }
                                            buildEvent.setMatiere(categorie[1]);
                                        } else if (categorie[0].contains("Enseignant")) {
                                            buildEvent.setEnseignant(categorie[1]);
                                        } else if (categorie[0].contains("Promotions") || categorie[0].contains("TD")) {
                                            List<Integer> tabGroupe = DBEvenement.promoStringTabToInt(categorie[1].split("\\,"));
                                            buildEvent.setPromotion(tabGroupe);
                                        } else if (categorie[0].contains("Type")) {
                                            buildEvent.setType(categorie[1]);
                                        } else if (categorie[0].contains("Mémo")) {
                                            buildEvent.setMemo(categorie[1]);
                                        }
                                    }
                                }
                            } else if (ligne.split(":")[0].contains("LOCATION")) {
                                buildEvent.setLieu(ligne.split(":")[1]);
                            }
                            ligne = sc.nextLine();
                            if (annuler) {
                                buildEvent = null;
                                while (!ligne.equals("END:VEVENT")) {
                                    sc.nextLine();
                                }
                            }
                        }
                        Evenement evenement = buildEvent.buildEvenement();
                        Log.e("EVENT_INIT", evenement.toString());
                        this.dbEvenement.addEvent(evenement);
                    }
                }
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("DEBUG", "ahahahaha");
    }
}
