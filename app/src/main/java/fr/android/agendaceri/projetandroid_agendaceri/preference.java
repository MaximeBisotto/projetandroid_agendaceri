package fr.android.agendaceri.projetandroid_agendaceri;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fr.android.agendaceri.projetandroid_agendaceri.R;

public class preference extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        Button valider = (Button) findViewById(R.id.buttonValidation);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
                String codeDiplome = ( (TextView) findViewById(R.id.labelCodeDiplome)).getText().toString();
                sharedPreferences.edit().putString("codeDiplome", codeDiplome).apply();
                int groupe = Integer.parseInt(( (EditText) findViewById(R.id.labelCodeDiplome)).getText().toString());
                sharedPreferences.edit().putInt("groupe", groupe).apply();
                Intent intent = new Intent(preference.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
