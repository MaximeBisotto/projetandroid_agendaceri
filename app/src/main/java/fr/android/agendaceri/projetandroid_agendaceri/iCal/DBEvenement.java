package fr.android.agendaceri.projetandroid_agendaceri.iCal;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.LinearLayout;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import fr.android.agendaceri.projetandroid_agendaceri.Edt;
import fr.android.agendaceri.projetandroid_agendaceri.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class DBEvenement extends SQLiteOpenHelper {
    public final String URL_AGENDA_CONST = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/";
    private String urlAgenda;
    private int groupeDefault;

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "cours.db";

    public static final String TABLE_NAME = "cours";

    public static final String COLUMN_MATIERE = "matiere";
    public static final String COLUMN_LIEU = "lieu";
    public static final String COLUMN_DATE_DEBUT = "dateDebut";
    public static final String COLUMN_DATE_FIN = "dateFin";
    public static final String COLUMN_ENSEIGNANT = "enseignant";
    public static final String COLUMN_PROMOTION = "promotion";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_MEMO = "memo";


    public DBEvenement(Context context, SharedPreferences sharedPreferences) throws Exception {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        if (sharedPreferences.contains("codeDiplome")) {
            this.urlAgenda = URL_AGENDA_CONST + sharedPreferences.getString("codeDiplome", "2-L3IN");
            this.groupeDefault = sharedPreferences.getInt("groupe", 3);
        }
        else {
            this.urlAgenda = URL_AGENDA_CONST + "2-L3IN";
            this.groupeDefault = 3;
        }
        NetworkInfo network = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (network != null && network.isConnected()) {
            onCreate(this.getReadableDatabase());
            RecupICal recupICal = new RecupICal(urlAgenda, this);
            recupICal.execute();
        }
    }

    public static List<Integer> promoStringTabToInt(String[] tabGroupeString) {
        List<Integer> listPromo = new ArrayList<>();
        for (String promo : tabGroupeString) {
            if (promo.contains(" ")) {
                for (int i = 1; i <= 9; ++i) {
                    listPromo.add(i);
                }
            }
            listPromo.add(Integer.parseInt(promo.split("_")[1].substring(2, 3)));
        }
        return listPromo;
    }

    @Deprecated
    public static Date stringToDate(String stringDate) {
        int annee = Integer.parseInt(stringDate.substring(0, 4));
        int mois = Integer.parseInt(stringDate.substring(5, 7));
        int jour = Integer.parseInt(stringDate.substring(6, 8));
        int heure = Integer.parseInt(stringDate.substring(10, 12));
        int minutes = Integer.parseInt(stringDate.substring(13, 15));
        Date date = new Date(annee, mois, jour, heure, minutes);
        return date;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String supprimeTable = "DROP TABLE IF EXISTS " + TABLE_NAME + " ;";
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_MATIERE + " TEXT NOT NULL, " +
                COLUMN_ENSEIGNANT + " TEXT NOT NULL, " +
                COLUMN_LIEU + " INTEGER, " +
                COLUMN_PROMOTION+ " TEXT, " +
                COLUMN_TYPE+ " TEXT, " +
                COLUMN_DATE_DEBUT+ " TEXT, " +
                COLUMN_DATE_FIN+ " TEXT, " +
                COLUMN_MEMO+ " TEXT);";
        sqLiteDatabase.execSQL(supprimeTable);
        sqLiteDatabase.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    public void addEvent(Evenement evenement) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_MATIERE, evenement.getMatiere());
        values.put(COLUMN_ENSEIGNANT, evenement.getEnseignant());
        values.put(COLUMN_LIEU, evenement.getLieu());
        values.put(COLUMN_PROMOTION, evenement.promoToString());
        values.put(COLUMN_TYPE, evenement.getType());
        values.put(COLUMN_DATE_DEBUT, evenement.getDebut().toString());
        values.put(COLUMN_DATE_FIN, evenement.getFin().toString());
        values.put(COLUMN_MEMO, evenement.getMemo());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public Evenement prochainCours() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(true, TABLE_NAME, new String[] {COLUMN_MATIERE, COLUMN_ENSEIGNANT, COLUMN_LIEU, COLUMN_PROMOTION, COLUMN_TYPE, COLUMN_DATE_DEBUT, COLUMN_DATE_FIN, COLUMN_MEMO},COLUMN_DATE_DEBUT + " > DATE('now')", null, null, null, COLUMN_DATE_DEBUT + " ASC", "1");
        if (cursor != null) {
            cursor.moveToFirst();
            return cursorToEvent(cursor);
        }
        return null;
    }

    public Evenement prochaineEvaluation() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(true, TABLE_NAME, new String[] {COLUMN_MATIERE, COLUMN_ENSEIGNANT, COLUMN_LIEU, COLUMN_PROMOTION, COLUMN_TYPE, COLUMN_DATE_DEBUT, COLUMN_DATE_FIN, COLUMN_MEMO},COLUMN_DATE_DEBUT + " > DATE('now') AND " + COLUMN_TYPE + " = ", new String[]{"Evaluation"}, null, null, COLUMN_DATE_DEBUT + " ASC", "1");
        if (cursor != null) {
            cursor.moveToFirst();
            return cursorToEvent(cursor);
        }
        return null;
    }

    public List<Evenement> coursAtDate(Date date) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Evenement> tabEvent = new ArrayList<>();
        Cursor cursor = db.query(true, TABLE_NAME, new String[] {COLUMN_MATIERE, COLUMN_ENSEIGNANT, COLUMN_LIEU, COLUMN_PROMOTION, COLUMN_TYPE, COLUMN_DATE_DEBUT, COLUMN_DATE_FIN, COLUMN_MEMO},null, null, null, null, COLUMN_DATE_DEBUT + " ASC", null);
        if (cursor != null) {
            cursor.moveToFirst();
            tabEvent.add(cursorToEvent(cursor));
        }
        return tabEvent;
    }

    public List<Evenement> alerteEvaluation(Date date) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Evenement> tabEvent = new ArrayList<>();
        Cursor cursor = db.query(true, TABLE_NAME, new String[] {COLUMN_MATIERE, COLUMN_LIEU, COLUMN_TYPE, COLUMN_DATE_DEBUT, COLUMN_DATE_FIN},COLUMN_DATE_DEBUT + " > DATE('now') AND strftime('%d','now') - strftime('%d'," + COLUMN_DATE_DEBUT + ") < 14 AND " + COLUMN_TYPE + " = ", new String[]{"Evaluation"}, null, null, COLUMN_DATE_DEBUT + " ASC", "1");
        if (cursor != null) {
            cursor.moveToFirst();
            tabEvent.add(cursorToEvent(cursor));
        }

        return tabEvent;
    }

    private Evenement cursorToEvent(Cursor cursor) {
        if (cursor != null) {
            return new Evenement(cursor.getString(0), cursor.getString(1), cursor.getString(2), Evenement.toPromotion(cursor.getString(3)), cursor.getString(4), cursor.getString(7), new Date(cursor.getString(5)), new Date(cursor.getString(6)));
        }
        return null;

    }
}
